module.exports = {
    devServer: {
        proxy: {
            '/doLogin': {
                target: 'localhost:3000',
                // ws: true,		//websocket
                changeOrigin: true
            },
            '/getData': {
                target: 'localhost:3000',
                // ws: true,		//websocket
                changeOrigin: true
            }
        }
    },
    pluginOptions: {
        electronBuilder: {
            // List native deps here if they don't work
            nodeIntegration: true,
            builderOptions: {
                "productName": "待配药",
                "appId": "com.zhh.hall-screen",
                "copyright": "Copyright © 2023 ZHH",
                "electronDownload": {
                    "mirror": "https://npmmirror.com/mirrors/electron/"
                },
                "win": {
                    "icon": "./src/assets/package.png",
                    "target": [
                        {
                            "target": "nsis",
                            "arch": ["ia32"]
                        }
                    ]
                },
                // 自定义打包路径需要配置nsis
                "nsis": {
                    "include": "installer.nsh",
                    "oneClick": false,  // 是否创建一键安装程序或辅助
                    "perMachine": true,  // 是否显示辅助安装程序的安装模式安装程序页面（选择每台机器或每用户）
                    "allowElevation": true,
                    "allowToChangeInstallationDirectory": true,  // 是否允许用户更改安装目录。
                    // "installerIcon": "./src/assets/package.png",  // 安装程序图标的路径
                    // "uninstallerIcon": "./src/assets/package.png",  // 卸载程序图标的路径
                    // "installerHeaderIcon": "./src/assets/package.png",  // 标题图标的路径（进度条上方）
                    "createDesktopShortcut": true,  // 是否创建桌面快捷方式
                    "createStartMenuShortcut": true,  // 是否创建开始菜单快捷方式
                    "shortcutName": "待配药",  // 默认为应用程序名称
                    "runAfterFinish": false  // 完成后是否运行已安装的应用程序
                },
                "extraResources": ["./static/**"]
            }
        }
    }
}
