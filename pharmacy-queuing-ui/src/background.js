'use strict'

// eslint-disable-next-line no-unused-vars
import { app, ipcMain, protocol, screen, BrowserWindow } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
const isDevelopment = process.env.NODE_ENV !== 'production'
const fs = require("fs")

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])


function createWindow() {
  // let size = screen.getPrimaryDisplay().workAreaSize;
  // let sWidth = size.width;
  // let sHeight = size.height;
  let sWidth = 500;
  let sHeight = 300;
  let isFullscreen = false;
  win = new BrowserWindow({

    width: sWidth,
    height: sHeight,
    // backgroundColor: '#c8d8f5',
    // kiosk: true,  //服务亭模式
    // frame: true,  //是否显示窗口边缘框架
    autoHideMenuBar: true,
    fullscreen: isFullscreen,  //全屏窗口
    resizable: false, //调整窗口大小
    maximizable: true, //支持最大化
    show: false, //为了让初始化窗口显示无闪烁，先关闭显示，等待加载完成后再显示。
    webPreferences: {
      nodeIntegration: true,    //允许运行node环境
      enableRemoteModule: true, //允许渲染进程调用remote
      webSecurity: false,        //去除跨域限制
      webviewTag: true
    }
  })

  win.once('ready-to-show', () => {
    win.show();
  })
  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (isDevelopment) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }

  win.on('closed', () => {
    win = null
  })
}

const gotTheLock = app.requestSingleInstanceLock();
if (!gotTheLock) {
  app.quit();
} else {
  // eslint-disable-next-line no-unused-vars
  app.on('second-instance', (event, commandLine, workingDirectory) => {
    // 当运行第二个实例时,将会聚焦到myWindow这个窗口
    if (win) {
      if (win.isMinimized()) {
        win.restore();
      }
      win.focus();
    }
  })

  //当所有窗口都被关闭后退出
  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit();
    }
  })
}

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

//取消硬件加速，避免一些配置低的机器白屏问题
app.disableHardwareAcceleration();

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}

ipcMain.on('getPrinterDefaultName', (event) => {
  //监听获取获取默认打印机名称
  const list = win.webContents.getPrinters();
  let name = ''
  for (let item of list) {
    item.isDefault && (name = item.name)
  }
  console.log(name)
  event.returnValue = name;
});

// 开机自启动
ipcMain.on('AUTOSTARTAPP', (event, args) => {
  app.setLoginItemSettings({
    openAtLogin: args,
    path: process.execPath,
    args: []
  });
});
