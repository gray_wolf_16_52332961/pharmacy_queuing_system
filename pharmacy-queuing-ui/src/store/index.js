import Vue from 'vue'
import Vuex from 'vuex'
const fs = require('fs')

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        user: null,
        scholarDataX:null,
        organId:JSON.parse(fs.readFileSync(process.env.VUE_APP_CONFIG)).organId
    },
    mutations: {
        saveUser (state, payload) {
            if(!payload){   //清空user
                state.user = payload;
            }else if(payload.accountId){
                state.user = {};
                state.user.accountId = payload.accountId;
                state.user.userName = payload.userName;
                state.user.userId = payload.userId;
                state.user.paculty = payload.paculty;
                state.user.special = payload.special;
            }else if(payload.year){
                state.user.year = payload.year;
                state.user.scholarName = payload.scholarName;
            }
        },
        saveScholarDataX(state,payload){
            state.scholarDataX = payload
        }
    }
})

export default store

