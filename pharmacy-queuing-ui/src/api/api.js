import axios from 'axios'
// import log from '@/plugins/log.js';

axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'

// 当前大厅等待人数
export function getNowWait() {
  return axios.get(`http://127.0.0.1:8060/api/nowWait`);
}

// 今日总取号量
export function getTotalQh() {
  return axios.get(`http://127.0.0.1:8060/api/totalQh`);
}

// 大厅办理人数
export function getBlrs() {
  return axios.get(`http://127.0.0.1:8060/api/blrs`);
}

// 窗口和业务数量
export function getWindowsAndBusiness() {
  return axios.get(`http://127.0.0.1:8060/api/getWindowsAndBusiness`);
}

// 今日业务取号量
export function getYwQhNum() {
  return axios.get(`http://127.0.0.1:8060/api/getYwQhNum`);
}

// 今日业务办理量
export function getYwNum() {
  return axios.get(`http://127.0.0.1:8060/api/getYwNum`);
}

// 办理统计
export function getBltj() {
  return axios.get(`http://127.0.0.1:8060/api/getBltj`);
}

// 各时间段人流量
export function getRll() {
  return axios.get(`http://127.0.0.1:8060/api/getRll`);
}