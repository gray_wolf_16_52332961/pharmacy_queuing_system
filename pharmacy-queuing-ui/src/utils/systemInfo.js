const os = require('os');
const { exec } = require('child_process');
const networksObj = os.networkInterfaces();
// const networkRes = networksObj["以太网"] && networksObj["以太网"][0];
const networkRes = networksObj["WLAN"] && networksObj["WLAN"][0];

//获取IP地址
function getLocalIP() {
  return networkRes.address;
}

//获取mac地址
function getLocalMac() {
  return networkRes.mac.replaceAll(":", "").toUpperCase();
}

//获取计算机名称
function getPCName() {
  return os.hostname();
}

//获取cpu序列号
function getCpuNum() {
  return new Promise((resolve, reject) => {
    let command = '';
    switch (process.platform) {
      case 'win32':
        command = 'wmic cpu get ProcessorId';
        break;
      case 'linux':
        command = 'cat /proc/cpuinfo | grep Serial | awk \'{print $NF}\'';
        break;
      case 'darwin':
        command = 'system_profiler SPHardwareDataType | grep "Serial Number" | awk \'{print $NF}\'';
        break;
      default:
        reject(new Error(`Unsupported platform: ${process.platform}`));
    }
    exec(command, (err, stdout) => {
      if (err) {
        reject(err);
      } else {
        resolve(stdout.trim());
      }
    });
  });
}

function getLocalCpuNum() {
  return getCpuNum().then((serialNumber) => {
    var str = serialNumber.split("\r\r\n");
    return str[1];
  }).catch((err) => {
    console.error(`Failed to get CPU serial number: ${err.message}`);
    return err.message;
  })
}

function getHardDisk() {
  return new Promise((resolve, reject) => {
    let command = '';
    switch (process.platform) {
      case 'win32':
        command = 'wmic logicaldisk get Caption,FreeSpace,Size,VolumeSerialNumber,Description,FileSystem  /format:list';
        break;
      case 'linux':
        reject(new Error('未知'));
        break;
      case 'darwin':
        reject(new Error('未知'));
        break;
      default:
        reject(new Error(`Unsupported platform: ${process.platform}`));
    }
    exec(command, (err, stdout) => {
      if (err) {
        reject(err);
      } else {
        resolve(stdout.trim());
      }
    });
  });
}

function getSysHardDisk() {
  return getHardDisk().then((sysHardDisk) => {
    var str = sysHardDisk.split("\r\r\n");
    var result = {
      mount: str[0],
      type: str[2],
      size: str[4],
      number: str[5]
    };
    var sysName = "";
    switch (process.platform) {
      case 'win32':
        sysName = "Windows";
        break;
      case 'linux':
        sysName = "Linux";
        break;
      case 'darwin':
        sysName = "Darwin";
        break;
      default:
        "";
    }
    var tmpMount = result.mount.split("=")[1];
    var tmpNumber = result.number.split("=")[1];
    return (sysName + " " + tmpMount + "-" + tmpNumber).replaceAll(":", "");
  }).catch((err) => {
    console.error(`Failed to get hardSerial: ${err.message}`);
    return err.message;
  })
}

function getHardInfo() {
  return getHardDisk().then((sysHardDisk) => {
    var str = sysHardDisk.split("\r\r\n");
    var result = {
      mount: str[0],
      type: str[2],
      size: str[4],
      number: str[5]
    };

    var tmpMount = result.mount.split("=")[1];
    var tmpNumber = result.number.split("=")[1];
    var tmpType = result.type.split("=")[1];
    var tmpSize = result.size.split("=")[1];
    tmpSize = Math.floor((tmpSize / (1024 * 1024 * 1024)));
    return (tmpMount + "^" + tmpNumber + "^" + tmpType + "^" + tmpSize + "G").replaceAll(":", "");
  }).catch((err) => {
    console.error(`Failed to get hardSerial: ${err.message}`);
    return err.message;
  })
}

function getHardSerial() {
  return new Promise((resolve, reject) => {
    let command = '';
    switch (process.platform) {
      case 'win32':
        command = 'wmic diskdrive get SerialNumber';
        break;
      case 'linux':
        command = 'lsblk -d -n -o serial';
        break;
      case 'darwin':
        reject(new Error('未知'));
        break;
      default:
        reject(new Error(`Unsupported platform: ${process.platform}`));
    }
    exec(command, (err, stdout) => {
      if (err) {
        reject(err);
      } else {
        resolve(stdout.trim());
      }
    });
  });
}

function getLocalHardSerial() {
  return getHardSerial().then((hardSerial) => {
    var str = hardSerial.split("\r\r\n")[1];
    if (str.indexOf("_") != -1) {
      str = str.replaceAll("_", "-");
    }
    if (str.length > 24) {
      str = str.substring(5, 24);
    } else {
      str = str.substring(0, str.length - 1);
    }
    return str;
  }).catch((err) => {
    console.error(`Failed to get hardSerial: ${err.message}`);
    return err.message;
  })
}

function getKdbrowserRate() {
  return "0";
}

export default {
  getLocalIP,
  getLocalMac,
  getPCName,
  getLocalCpuNum,
  getSysHardDisk,
  getHardInfo,
  getLocalHardSerial,
  getKdbrowserRate
}