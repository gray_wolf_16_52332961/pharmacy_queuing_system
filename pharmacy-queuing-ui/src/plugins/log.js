const log4js = require('log4js');
const { remote } = require('electron');
const appPath = process.cwd();

let render = 'main';
if (remote) {
    render = 'render';
} else {
    render = 'main';
}

log4js.configure({
    appenders: {
        consoleOut: {
            type: "console"
        },
        infoOut: {
            type: 'dateFile',
            filename: `${appPath}/logs/${render}_info`,
            pattern: 'yyyy-MM-dd.log',
            daysToKeep: 7,
            alwaysIncludePattern: true
        },
        errorOut: {
            type: 'dateFile',
            filename: `${appPath}/logs/${render}_error`,
            pattern: 'yyyy-MM-dd.log',
            daysToKeep: 7,
            alwaysIncludePattern: true
        }
    },
    categories: {
        info: {
            appenders: ['infoOut', 'consoleOut'],
            level: 'info'
        },
        error: {
            appenders: ['errorOut', 'consoleOut'],
            //显示debug以上级别的日志
            level: 'debug'
        },
        default: {
            appenders: ['consoleOut'],
            level: 'debug'
        }
    }
});

module.exports = {
    info: msg => {
        log4js.getLogger('info').info(msg);
    },
    error: msg => {
        //输出error级别日志
        log4js.getLogger('error').error(msg);
    },
    debug: msg => {
        //调用categories为error的配置输出debug级别日志，如果该配置level级别高于dubug，则该日志不显示
        //error>warn>info>debug
        log4js.getLogger('error').debug(msg);
    }
};