package com.zhh.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.zhh.web.domain.AjaxResult;
import com.zhh.web.domain.Dispensary;
import com.zhh.web.domain.HerbMedicine;
import com.zhh.web.domain.vo.DispensaryAddVO;
import com.zhh.web.service.DispensaryService;
import com.zhh.web.service.HerbMedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
public class ApiController {

    @Autowired
    private DispensaryService dispensaryService;

    @Autowired
    private HerbMedicineService herbMedicineService;

    /**
    * description: 待取药（报道机从HIS取到签到信息后调用）
    * @date 2024-03-30 14:14
    * @author zhh
    */
    @PostMapping("/setDqy")
    public AjaxResult setDqy(@RequestBody Dispensary dispensary) throws Exception {
        dispensaryService.save(dispensary);
        return AjaxResult.success();
    }

    /**
    * description: 已发药（HIS发药完的回调接口）
    * @date 2024-03-30 14:14
    * @author zhh
    */
    @PostMapping("/completeFy")
    public AjaxResult completeFy(@RequestBody DispensaryAddVO dispensaryAddVO) throws Exception {
        LambdaQueryWrapper<Dispensary> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(Dispensary::getCardNumber, dispensaryAddVO.getCard_number())
                .eq(Dispensary::getPatientId, dispensaryAddVO.getPatient_id())
                .eq(Dispensary::getPharmacyId, dispensaryAddVO.getPharmacy_id())
                .eq(Dispensary::getPharmacyWindowId, dispensaryAddVO.getPharmacy_window_id());
        dispensaryService.remove(queryWrapper);
        return AjaxResult.success();
    }

    /**
    * description: 草药房-配药中
    * @date 2024-03-30 15:13
    * @author zhh
    */
    @PostMapping("/setPyz")
    public AjaxResult setPyz(@RequestBody HerbMedicine herbMedicine) throws Exception {
        herbMedicineService.save(herbMedicine);
        return AjaxResult.success();
    }

    /**
    * description: 配药完成，双击进入待取药列表
    * @date 2024-03-30 16:30
    * @author zhh
    */
    @Transactional
    @GetMapping(value = "/{id}")
    public AjaxResult getById(@PathVariable("id") Long id) {
        HerbMedicine herbMedicine = herbMedicineService.getById(id);
        Dispensary dispensary = new Dispensary();
        dispensary.setPresNo(herbMedicine.getPresNo());
        dispensary.setPresTime(herbMedicine.getPresTime());
        dispensary.setCardNumber(herbMedicine.getCardNumber());
        dispensary.setCardType(herbMedicine.getCardType());
        dispensary.setPatientId(herbMedicine.getPatientId());
        dispensary.setPatientName(herbMedicine.getPatientName());
        dispensary.setPharmacyId(herbMedicine.getPharmacyId());
        dispensary.setPharmacyName(herbMedicine.getPharmacyName());
        dispensary.setPharmacyWindowId(herbMedicine.getPharmacyWindowId());
        dispensary.setPharmacyWindowName(herbMedicine.getPharmacyWindowName());
        dispensary.setPayTime(herbMedicine.getPayTime());
        dispensaryService.save(dispensary);
        return AjaxResult.success();
    }

}
