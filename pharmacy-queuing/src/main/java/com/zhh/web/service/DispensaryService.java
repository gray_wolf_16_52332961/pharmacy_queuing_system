package com.zhh.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhh.web.domain.Dispensary;

/**
 * description:
 *
 * @author zhh
 * @date 2024-03-29 16:12
 */
public interface DispensaryService extends IService<Dispensary> {

}
