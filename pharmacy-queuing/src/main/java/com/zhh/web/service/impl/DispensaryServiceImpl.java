package com.zhh.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhh.web.domain.Dispensary;
import com.zhh.web.mapper.DispensaryMapper;
import com.zhh.web.service.DispensaryService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * description:
 *
 * @author zhh
 * @date 2024-03-29 16:13
 */
@Service
public class DispensaryServiceImpl extends ServiceImpl<DispensaryMapper, Dispensary> implements DispensaryService {

}
