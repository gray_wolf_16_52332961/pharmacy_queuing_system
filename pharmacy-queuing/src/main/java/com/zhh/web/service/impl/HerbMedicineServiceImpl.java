package com.zhh.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhh.web.domain.Dispensary;
import com.zhh.web.domain.HerbMedicine;
import com.zhh.web.mapper.DispensaryMapper;
import com.zhh.web.mapper.HerbMedicineMapper;
import com.zhh.web.service.DispensaryService;
import com.zhh.web.service.HerbMedicineService;
import org.springframework.stereotype.Service;

/**
 * description:
 *
 * @author zhh
 * @date 2024-03-29 16:13
 */
@Service
public class HerbMedicineServiceImpl extends ServiceImpl<HerbMedicineMapper, HerbMedicine> implements HerbMedicineService {

}
