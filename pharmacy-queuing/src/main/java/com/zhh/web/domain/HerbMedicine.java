package com.zhh.web.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class HerbMedicine {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String presNo;

    private Date presTime;

    private String cardNumber;

    private Integer cardType;

    private String patientId;

    private String patientName;

    private String pharmacyId;

    private String pharmacyName;

    private String pharmacyWindowId;

    private String pharmacyWindowName;

    private Date payTime;

}
