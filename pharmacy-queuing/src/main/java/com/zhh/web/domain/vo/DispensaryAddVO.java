package com.zhh.web.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class DispensaryAddVO {

    private String card_number;

    private String patient_id;

    private String pharmacy_id;

    private String pharmacy_window_id;

}
