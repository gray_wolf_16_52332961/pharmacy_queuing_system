package com.zhh.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhh.web.domain.Dispensary;
import com.zhh.web.domain.HerbMedicine;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * description:
 *
 * @author zhh
 * @date 2024-03-29 16:11
 */
@Mapper
@Repository
public interface HerbMedicineMapper extends BaseMapper<HerbMedicine> {
}
